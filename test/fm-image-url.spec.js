
describe('fmImageUrl', function() {
  'use strict';


  it('should return the transformed url', function() {
    module('fmImageUrl', function(fmImageUrlProvider) {
      fmImageUrlProvider
        .setBucketName('bucket')
        .addImageType('mini', [ { width: 100, height: 200 }]);
    });

    inject(function(fmImageUrl) {
      var result = fmImageUrl({}, 'testID123', 'mini', '.gif');

      expect(result).toBe('http://res.cloudinary.com/bucket/image/upload/w_100,h_200/testID123.gif');
    });
  });

  it('should return complex transformed url', function() {
    module('fmImageUrl', function(fmImageUrlProvider) {
      fmImageUrlProvider
        .setBucketName('unit-test-2')
        .addImageType('full', [ { width: 50, height: 50 }, { crop: 'fill', gravity: 'face', radius: 12 }]);
    });

    inject(function(fmImageUrl) {
      expect(fmImageUrl({}, 'testing', 'full')).toBe(
        'http://res.cloudinary.com/unit-test-2/image/upload/w_50,h_50/c_fill,g_face,r_12/testing.png');
    });
  });

  it('should return url with adjusted size if the percentage size is set', function() {
    module('fmImageUrl', function(fmImageUrlProvider) {
      fmImageUrlProvider
        .setBucketName('bucket')
        .addImageType('perc', [ { width: '50%', height: '20%' }]);
    });

    inject(function(fmImageUrl) {
      expect(fmImageUrl({ offsetWidth: 400, offsetHeight: 500 }, 'testing', 'perc')).toBe(
        'http://res.cloudinary.com/bucket/image/upload/w_200,h_100/testing.png');
    });
  });

  it('should return default placeholder when cloud id is not provided', function() {
    module('fmImageUrl', function(fmImageUrlProvider) {
      fmImageUrlProvider
        .setBucketName('bucket')
        .setPlaceholderId('PLACEHOLDER')
        .addImageType('any', [ { width: 50 } ]);
    });

    inject(function(fmImageUrl) {
      expect(fmImageUrl({}, null, 'any')).toBe(
        'http://res.cloudinary.com/bucket/image/upload/w_50/PLACEHOLDER.png');
    });
  });

  it('should throw exception if cloud id is not provided and no placeholder id set', function() {
    module('fmImageUrl', function(fmImageUrlProvider) {
      fmImageUrlProvider
        .setBucketName('any')
        .addImageType('any', [ { width: 50 } ]);
    });

    inject(function(fmImageUrl) {
      expect(function() { fmImageUrl({}, null, 'any') }).toThrow('No placeholder defined');
    });
  });

  it('should throw exception if bucket is not defined', function() {
    module('fmImageUrl', function(fmImageUrlProvider) {
      fmImageUrlProvider.addImageType('any', [ { width: 50 } ]);
    });

    inject(function(fmImageUrl) {
      expect(function() { fmImageUrl({}, 'id', 'any') }).toThrow('No bucket name defined');
    })
  });
});