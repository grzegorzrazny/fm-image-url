

angular.module('fmImageUrl', [ ])
  .directive('fmImageUrl', function($window, $timeout, fmImageUrl) {
    'use strict';

    return function(scope, elem, attrs) {
      var parent = elem[0].parentNode;

      function refresh() {
        var imageId = scope.$eval(attrs.fmImageUrl);
        attrs.$set(attrs.targetAttribute || 'src', fmImageUrl(parent, imageId, attrs.fmImageType));
      }


      scope.$watch(attrs.fmImageUrl, refresh);
      scope.$watch(function() {
        return parent.offsetHeight + 'x' + parent.offsetWidth;
      }, function(newVal, oldVal) {
        if (newVal !== oldVal) {
          refresh();
        }
      });
    };

  })

  .provider('fmImageUrl', function() {
    'use strict';
    var prefixMapping = {
      'effect': 'e',
      'width': 'w',
      'height': 'h',
      'angle': 'a',
      'radius': 'r',
      'gravity': 'g',
      'crop': 'c'
    };

    var imageTypes = {};
    var bucketName, placeholderId;

    this.addImageType = function(name, transformations) {
      imageTypes[name] = transformations;
      return this;
    };
    this.setBucketName = function(name) {
      bucketName = name;
      return this;
    };
    this.setPlaceholderId = function(id) {
      placeholderId = id;
      return this;
    };

    function prepareItem(element, prop, value) {
      if (angular.isString(value) && value.charAt(value.length - 1) === '%' && (prop === 'width' || prop ==='height')) {
        if (prop === 'width') {
          return element.offsetWidth * 0.01 * value.substr(0, value.length - 1);
        } else {
          return element.offsetHeight * 0.01 * value.substr(0, value.length - 1);
        }
      }
      return value;
    }

    function formatTransformations(transformations, element) {
      return transformations.map(function(item) {
        var transElems = [];

        for (var prop in item) {
          if (item.hasOwnProperty(prop)) {
            var prefix = prefixMapping[prop];
            if (prefix) {
              transElems.push(prefix + '_' + prepareItem(element, prop, item[prop]));
            }
          }
        }

        return transElems.join(',');
      }).join('/');
    }

    this.$get = function() {
      return function (element, cloudId, imageType, extension) {
        if (!bucketName) throw 'No bucket name defined';

        var url = 'http://res.cloudinary.com/' + bucketName + '/image/upload/';

        url += formatTransformations(imageTypes[imageType || 'default'], element) + '/';

        url += cloudId ? cloudId : (placeholderId ? placeholderId : function() {
          throw 'No placeholder defined'
        }());
        url += extension ? extension : '.png';

        return url;
      };
    };
  });
